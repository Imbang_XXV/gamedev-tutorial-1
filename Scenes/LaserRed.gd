extends Area2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
export var speed = 100
var screensize

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
	position.y += speed * delta
	screensize = get_viewport_rect().size
	if position.y > 2050:
		queue_free()

func _on_Laser_area_entered(area):
	if area.is_in_group("player"):
		area.queue_free()
		queue_free()


func _on_LaserRed_area_entered(area):
	pass # replace with function body
